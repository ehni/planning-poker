<div id="top"></div>
<br />
<div align="center">

  <h3 align="center">Planning Poker</h3>

  <p align="center">
    Estimate without distractions, ads or paywalls.
    <br />
    <br />
    <a href="https://the-planning-poker.de" target="_blank">Open Planning Poker</a>
    ·
    <a href="https://gitlab.com/ehni/planning-poker/issues" target="_blank">Report Bug</a>
    ·
    <a href="https://gitlab.com/ehni/planning-poker/issues" target="_blank">Request Feature</a>
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

[![Planning Poker Screen Shot][product-screenshot]](/images/screenshot.png)

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

<ul>
  <li><a href="https://adonisjs.com" target="_blank">Adonis.js</a></li>
  <li><a href="https://vuejs.org" target="_blank">Vue.js</a></li>
  <li><a href="https://tailwindcss.com" target="_blank">Tailwind.css</a></li>
  <li><a href="https://socket.io" target="_blank">Socket.io</a></li>
  <li><a href="https://www.npmjs.com/package/canvas-confetti" target="_blank">canvas-confetti</a></li>
</ul>

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

Requirements

- `yarn`
- `node 17`

### Installation

1. Clone the repo
   ```bash
   git clone https://gitlab.com/ehni/planning-poker
   ```
2. Install packages packages
   ```bash
   yarn install
   # install husky for commit linting
   yarn installHusky
   ```
3. Create `tmp` directory for the sqlite database in local development
   ```bash
   mkdir tmp
   ```
4. Run migrations for database ([→ AdonisJS migration documentation](https://docs.adonisjs.com/guides/database/migrations#run--rollback-migrations))

   ```bash
   # execute migrations
   node ace migration:run

   # completely reset database
   # node ace migration:reset

   # seed db
   node ace db:seed
   ```

5. Start for development
   ```bash
   yarn dev
   ```
6. Build & run production mode
   ```bash
   yarn build
   yarn start
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

## Roadmap

See the [issue board](https://gitlab.com/ehni/planning-poker/-/boards) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

`LICENSE.txt` includes ISC license from [`canvas-confetti`](https://www.npmjs.com/package/canvas-confetti).

<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

Robert Ehni - hallo@robert-ehni.de

Project Link: [https://gitlab.com/ehni/planning-poker](https://gitlab.com/ehni/planning-poker)

<p align="right">(<a href="#top">back to top</a>)</p>

## Acknowledgments
