/* eslint-disable */
const fs = require('fs')
const fileName = './build/package.json'
const packageJSON = JSON.parse(fs.readFileSync(fileName).toString())
delete packageJSON.scripts.postinstall
fs.writeFileSync(fileName, JSON.stringify(packageJSON))
