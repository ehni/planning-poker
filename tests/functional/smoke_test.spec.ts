import { test } from '@japa/runner'

test('display content', async ({ client }) => {
  const response = await client.get('/')

  response.assertStatus(200)
  response.assertTextIncludes('<title>The Planning Poker</title>')
})
