import { test } from '@japa/runner'
import { createUUID } from 'App/Util/UUIDGenerator'

test.group('Util uuid generator', () => {
  test('Should create some kind of random uuids', ({ assert }) => {
    const uuidArray: string[] = []
    for (let index = 0; index < 20; index++) {
      const newUUID = createUUID()
      assert.isFalse(uuidArray.includes(newUUID), 'UUID already generated')
      uuidArray.push(newUUID)
    }
  })
})
