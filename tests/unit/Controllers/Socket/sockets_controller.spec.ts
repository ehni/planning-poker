import { test } from '@japa/runner'
import SocketsController from 'App/Controllers/Socket/SocketsController'

test.group('SocketsController.createNewSession', () => {
  test('should create new session', ({ assert }) => {
    const name = 'Tom'
    const cardDeck = ['1', '2', '3']
    const socket = { join: () => {} }
    let returnedSession
    const callback = (data) => {
      assert.equal(data.name, name)
      returnedSession = data.session
    }

    SocketsController.createNewSession(socket, { name, cardDeck }, callback)

    SocketsController.addUserToSession({ id: name, join: () => {} }, returnedSession.code, name)

    const expectedSession = {
      code: returnedSession.code,
      creator: name,
      revealed: false,
      counted: false,
      cardDeck,
      createdAt: returnedSession.createdAt,
      user: [name],
      spectators: [],
      estimations: {},
      estimationStore: {},
    }

    assert.deepEqual(returnedSession, expectedSession)
  })

  test('should throw error when name missing', ({ assert }) => {
    const cardDeck = ['1', '2', '3']
    const callback = (data) => {
      assert.equal(data!.error, 'Name required')
    }
    SocketsController.createNewSession({}, { name: null, cardDeck }, callback)
  })

  test('should throw error when Object prototype function names are used as name', ({ assert }) => {
    const cardDeck = ['1', '2', '3']
    const objectPrototypePropertyNames = Object.getOwnPropertyNames(Object.prototype)
    for (const propertyName of objectPrototypePropertyNames) {
      const callback = (data) => {
        assert.equal(data!.error, 'This name is not allowed')
      }
      SocketsController.createNewSession({}, { name: propertyName, cardDeck }, callback)
    }
  })

  test('should not crash when callback is no function', ({ assert }) => {
    const name = 'Tom'
    const cardDeck = ['1', '2', '3']
    const callback = {}
    SocketsController.createNewSession({}, { name, cardDeck }, callback)
    assert.isTrue(true)
  })
})

test.group('SocketsController.joinSession', () => {
  test('should join session', ({ assert }) => {
    const creator = 'Jürgen'
    const name = 'Tom'
    const cardDeck = ['1', '2', '3']
    let sessionCode = null
    const creatorSocket = { id: creator, join: () => {} }
    const joinerSocket = { id: name, join: () => {} }
    let returnedSession
    const callback = (data) => {
      assert.equal(data.name, name)
      returnedSession = data.session
    }

    SocketsController.createNewSession(creatorSocket, { name: creator, cardDeck }, (data) => {
      sessionCode = data.session.code
    })

    SocketsController['broadcastToGroup'] = (code, event) => {
      assert.equal(code, sessionCode)
      assert.equal(event, 'server:updateSession')
    }

    SocketsController.joinSession(joinerSocket, { name, sessionCode }, callback)

    const expectedSession = {
      code: returnedSession.code,
      creator: creator,
      revealed: false,
      counted: false,
      cardDeck,
      createdAt: returnedSession.createdAt,
      user: [creator, name],
      spectators: [],
      estimations: {},
      estimationStore: {},
    }

    assert.deepEqual(returnedSession, expectedSession)
  })

  test('should throw error when name missing', ({ assert }) => {
    let sessionCode
    const cardDeck = ['1', '2', '3']
    const creator = 'someName'
    const socket = { join: () => {} }
    const callback = (data) => {
      assert.equal(data!.error, 'Name required')
    }
    SocketsController.createNewSession(socket, { name: creator, cardDeck }, (data) => {
      sessionCode = data.session.code
    })

    SocketsController.addUserToSession({ id: creator, join: () => {} }, sessionCode, creator)

    SocketsController.joinSession(socket, { name: null, sessionCode }, callback)
    SocketsController.joinSession(socket, { name: '', sessionCode }, callback)
  })
})

test.group('SocketsController.getSession', () => {
  test('should return session or throw error', ({ assert }) => {
    const creator = 'Tom'
    const cardDeck = ['1', '2', '3']
    const sessionCode = 'dummy123'

    const createdSession = SocketsController.createSession(sessionCode, cardDeck, creator)

    const expectedSession = createdSession
    const foundSession = SocketsController.sessionStore[sessionCode]

    foundSession.createdAt = expectedSession.createdAt

    assert.deepEqual(foundSession, expectedSession)
  })

  test('should throw error when session not found', ({ assert }) => {
    assert.plan(1)
    try {
      SocketsController.getSession('I do not exist')
    } catch (error) {
      assert.equal(error.message, 'Session not found')
    }
  })
})

test.group('SocketsController.createSession', () => {
  test('should create session in session & estimationStore', ({ assert }) => {
    const sessionCode = '123'
    const creator = 'Tom'
    const cardDeck = ['1', '2', '3']

    const newSession = SocketsController.createSession(sessionCode, cardDeck, creator)

    const expectedSession = {
      code: sessionCode,
      creator: creator,
      revealed: false,
      counted: false,
      cardDeck,
      createdAt: newSession!.createdAt,
      user: [creator],
      spectators: [],
      estimations: {},
      estimationStore: {},
    }

    assert.deepEqual(newSession, expectedSession)
  })
})

test.group('SocketsController.submitEstimation', () => {
  test('should update estimation for user', ({ assert }) => {
    const name = 'Tom'
    const sessionCode = '123'
    const estimation = 2
    const cardDeck = ['1', '2', '3']
    const newSession = SocketsController.createSession(sessionCode, cardDeck, name)

    assert.deepEqual(newSession.estimationStore, {})

    const callback = (response) => {
      assert.equal(response.estimation, estimation)
    }

    SocketsController['broadcastToGroup'] = (code, event) => {
      assert.equal(code, sessionCode)
      assert.equal(event, 'server:updateSession')
    }

    SocketsController.submitEstimation({ name }, { sessionCode, estimation }, callback)

    const session = SocketsController.sessionStore[sessionCode]

    assert.equal(session!.estimationStore[name], estimation)
    assert.equal(session!.estimations[name], 'X')
  })

  test('should delete estimation for user', ({ assert }) => {
    const name = 'Tom'
    const sessionCode = '123'
    const cardDeck = ['1', '2', '3']

    SocketsController.createSession(sessionCode, cardDeck, name)
    SocketsController['broadcastToGroup'] = () => {}
    SocketsController.submitEstimation({ name }, { sessionCode, estimation: 2 }, () => {})

    let session = SocketsController.sessionStore[sessionCode]

    assert.equal(session!.estimationStore[name], 2)

    const newEstimation = null

    const callback = (response) => {
      assert.equal(response.estimation, newEstimation)
    }

    SocketsController.submitEstimation(
      { name },
      { sessionCode, estimation: newEstimation },
      callback
    )

    session = SocketsController.sessionStore[sessionCode]

    assert.equal(session!.estimationStore[name], undefined)
    assert.equal(session!.estimations[name], undefined)
  })
})

test.group('SocketsController.socketDisconnected', () => {
  test('should make other player creator when creator leaves', ({ assert }) => {
    const sessionCode = 'sessionCode1'
    const creator = 'Tom'
    const cardDeck = ['1', '2', '3']

    const newSession = SocketsController.createSession(sessionCode, cardDeck, creator)

    SocketsController.addUserToSession({ id: creator, join: () => {} }, sessionCode, creator)

    let joinCalled = false
    const secondPlayerName = 'Hans'
    const secondPlayerSocket = {
      id: secondPlayerName,
      join: () => {
        joinCalled = true
      },
    }

    SocketsController['broadcastToGroup'] = () => {}

    SocketsController.joinSession(
      secondPlayerSocket,
      { name: secondPlayerName, sessionCode },
      () => {}
    )

    assert.deepEqual(newSession.creator, creator)

    let leaveCalled = false
    const creatorSocket = {
      id: creator,
      name: creator,
      sessionCode,
      leave: () => {
        leaveCalled = true
      },
    }

    SocketsController['broadcastToGroup'] = (code, event) => {
      assert.equal(code, sessionCode)
      assert.equal(event, 'server:updateSession')
    }

    SocketsController.socketDisconnected(creatorSocket)

    const session = SocketsController.sessionStore[sessionCode]

    assert.equal(session!.creator, secondPlayerName)
    assert.isTrue(joinCalled, '2nd player socket did not join')
    assert.isTrue(leaveCalled, 'creator socket did not leave')
  })

  test('should delete session when last player leaves', ({ assert }) => {
    const sessionCode = 'sessionCode'
    const creator = 'Tom'
    const cardDeck = ['1', '2', '3']

    SocketsController.createSession(sessionCode, cardDeck, creator)

    SocketsController.addUserToSession({ id: creator, join: () => {} }, sessionCode, creator)

    let leaveCalled = false
    const creatorSocket = {
      id: creator,
      name: creator,
      sessionCode,
      leave: () => {
        leaveCalled = true
      },
    }

    let updateSessionCalled = false
    SocketsController['broadcastToGroup'] = () => {
      updateSessionCalled = true
    }

    SocketsController.socketDisconnected(creatorSocket)

    const session = SocketsController.sessionStore[sessionCode]

    assert.equal(session, undefined)
    assert.isTrue(leaveCalled, 'creator socket did not leave')
    assert.isFalse(updateSessionCalled, 'updateSession should not have been called')
  })
})
