import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '',
    session: null,
    socket: null,
    darkMode: false,
    consensusConfetti: true,
  },
  getters: {
    name: (state) => state.name,
    socket: (state) => state.socket,
    session: (state) => state.session,
    darkMode: (state) => state.darkMode,
    consensusConfetti: (state) => state.consensusConfetti,
  },
  mutations: {
    initSocket(state) {
      state.socket = io()
      state.socket.on('server:updateSession', (session) => {
        state.session = session
      })
      state.socket.on('connect', () => {
        if (!state.name || !state.session) {
          return
        }
        console.log('Reconnecting')
        state.socket.emit(
          'client:reconnect',
          { name: state.name, sessionCode: state.session.code },
          (response) => {
            console.log('Reconnect response received')
            if (response?.error) {
              console.error('Error: ', response.error)
              alert(
                `Unable to reconnect after connection lost. Please try to rejoin the session. (${response.error})`
              )
              window.location = window.location.href
            }
            console.log('Successfully reconnected')
          }
        )
      })
    },
    emit(state, { eventName, payload, callback }) {
      state.socket.emit(eventName, payload, callback)
    },
    setName(state, name) {
      state.name = name
    },
    toggleDarkMode(store) {
      const appElement = document.getElementById('app')
      appElement.classList.toggle('dark')
      if (appElement.classList.contains('dark')) {
        localStorage.setItem('darkMode', true)
        store.darkMode = true
      } else {
        localStorage.removeItem('darkMode')
        store.darkMode = false
      }
    },
    toggleConsensusConfetti(store) {
      if (store.consensusConfetti) {
        localStorage.removeItem('consensusConfetti')
        store.consensusConfetti = false
      } else {
        localStorage.setItem('consensusConfetti', true)
        store.consensusConfetti = true
      }
    },
    setSession(state, session) {
      state.session = session
    },
  },
})
