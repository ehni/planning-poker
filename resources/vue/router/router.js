import VueRouter from 'vue-router'

import BuyMeACoffee from '../views/BuyMeACoffee.vue'
import Create from '../views/Create.vue'
import Imprint from '../views/Imprint.vue'
import Join from '../views/Join.vue'
import Session from '../views/Session.vue'
import Start from '../views/Start.vue'

export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Start },
    { path: '/create', component: Create },
    { path: '/join', component: Join },
    { path: '/session', component: Session },
    { path: '/imprint', component: Imprint },
    { path: '/buy-me-a-coffee', component: BuyMeACoffee },
  ],
})
