// app entrypoint
import '../css/main.css'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueNumber from 'vue-number-animation'
import App from '../vue/App'
import Vuex from 'vuex'
import store from '../vue/store/store'
import router from '../vue/router/router'

Vue.use(Vuex)
Vue.use(VueNumber)
Vue.use(VueRouter)

const update = (element, binding, vnode, oldVnode) => {
  element.style.visibility = binding.value ? '' : 'hidden'
}

Vue.directive('hide', {
  bind: update,
  update: update,
})

Vue.prototype.$sessionStats = window.sessionStats
Vue.prototype.$window = window

new Vue({
  el: '#app',
  components: { App },
  store,
  router,
  mounted() {},
  data: () => {
    return {}
  },
})
