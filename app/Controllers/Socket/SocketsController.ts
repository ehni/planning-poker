import { createUUID } from 'App/Util/UUIDGenerator'
import { DateTime } from 'luxon'
import Database from '@ioc:Adonis/Lucid/Database'
import Logger from '@ioc:Adonis/Core/Logger'
import Ws from 'App/Services/Ws'

function callbackIsNoFunction(functionName, data, callbackValue) {
  if (typeof callbackValue !== 'function') {
    Logger.error(
      `${functionName} called with invalid data: callback is no function! data: %o, callbackValue: %o`,
      data,
      callbackValue
    )
    return true
  }
}

export default class SocketsController {
  public static sessionStore = {}
  public static estimationStore = {}

  public static createSession(code: string, cardDeck: Array<string>, creator: string) {
    const newSession = {
      code: code,
      creator: creator,
      revealed: false,
      counted: false,
      spectators: [],
      cardDeck: cardDeck,
      estimations: {},
      estimationStore: {},
      user: [creator],
      createdAt: DateTime.now(),
    }
    this.sessionStore[code] = newSession
    this.estimationStore[code] = {}
    return newSession
  }

  public static getSession(sessionCode) {
    const session = this.sessionStore[sessionCode]
    if (!session) {
      throw new Error('Session not found')
    }
    return session
  }

  private static setEstimation(sessionCode: string, name: string, estimation: any) {
    const session = this.sessionStore[sessionCode]
    if (!session) {
      return
    }
    if (session.estimationStore[name] !== estimation && estimation !== null) {
      // Card selected, set estimation
      session.estimationStore[name] = estimation
      session.estimations[name] = 'X'
    } else {
      // User deselected card, delete estimation
      delete session.estimationStore[name]
      delete session.estimations[name]
    }
  }

  public static addUserToSession(socket: any, code: string, name: string) {
    socket.join(code)
    socket['name'] = name
    socket['sessionCode'] = code
    Logger.debug('Added %s to session %s', name, code)
  }

  private static broadcastToGroup(sessionCode: string, event: string, payload: Object) {
    Logger.debug('broadcasting %s to %s with %o', event, sessionCode, payload)
    Ws.io.to(sessionCode).emit(event, payload)
  }

  private static updateSessionForUsers(sessionCode: string) {
    const session = this.sessionStore[sessionCode]
    this.broadcastToGroup(sessionCode, 'server:updateSession', session)
  }

  public static toggleSpectatorMode(socket, { sessionCode }, callback) {
    try {
      if (callbackIsNoFunction('togglespectatorMode', { sessionCode }, callback)) return
      if (!socket.name) {
        throw new Error('Name required')
      }
      const session = this.getSession(sessionCode)
      if (session.spectators.includes(socket.name)) {
        session.spectators = session.spectators.filter((spectator) => spectator !== socket.name)
      } else {
        session.spectators.push(socket.name)
        this.setEstimation(sessionCode, socket.name, null)
      }
      this.updateSessionForUsers(sessionCode)
    } catch (error) {
      Logger.error(error)
      callback({ error: error.message || 'Error creating session' })
    }
  }

  public static createNewSession(socket, { name, cardDeck }, callback) {
    try {
      if (callbackIsNoFunction('createNewSession', { name }, callback)) return
      if (!name || !name.trim()) {
        throw new Error('Name required')
      }
      if (typeof {}[name.trim()] !== 'undefined') {
        throw new Error('This name is not allowed')
      }
      if (!cardDeck) {
        throw new Error('Card deck required')
      }
      Logger.debug('client:createSession for %s', name)
      const newCode = createUUID()
      this.createSession(newCode, cardDeck, name)
      this.addUserToSession(socket, newCode, name)
      Logger.debug('created session: %o', this.sessionStore[newCode])
      callback({ name, session: this.sessionStore[newCode] })
    } catch (error) {
      Logger.error(error)
      callback({ error: error.message || 'Error creating session' })
    }
  }

  public static submitEstimation(socket, { estimation, sessionCode }, callback) {
    Logger.debug(
      'client:submitEstimation from %s for %s with %s',
      socket['name'],
      sessionCode,
      estimation
    )
    try {
      if (callbackIsNoFunction('submitEstimation', { sessionCode, estimation }, callback)) return
      const session = this.getSession(sessionCode)
      if (session.revealed) {
        Logger.debug(
          'Race condition between estimation submit & estimation reveal. Prohibiting changing estimation'
        )
        return callback({
          error: 'Your estimation could not be submitted as the cards have been revealed already.',
        })
      }
      this.setEstimation(sessionCode, socket['name'], estimation)
      this.updateSessionForUsers(sessionCode)
      callback({ estimation })
    } catch (error) {
      Logger.error('error submitting estimation: ', error)
      return callback({ error: error.message || 'Error submitting estimation' })
    }
  }

  public static async revealEstimations(socket, { sessionCode }, callback) {
    try {
      Logger.debug('client:reveal from %s for %s', socket['name'], sessionCode)
      if (callbackIsNoFunction('revealEstimations', { sessionCode }, callback)) return
      const session = this.getSession(sessionCode)
      if (new Set([socket['name'], session.creator]).size !== 1) {
        throw new Error('Your are not the hoster')
      }
      session.revealed = true
      if (session.user.length > 1) {
        let dbSession = (await Database.from('session_counts').select('*'))[0]
        if (dbSession === undefined) {
          Logger.debug('Session count table empty, initializing')
          await Database.table('session_counts').insert({
            sessionCount: 0,
            estimationCount: 0,
            created_at: new Date(),
            updated_at: new Date(),
          })
          dbSession = (await Database.from('session_counts').select('*'))[0]
        }
        await Database.from('session_counts')
          .where('id', 1)
          .update({
            sessionCount: session.counted ? dbSession.sessionCount : dbSession.sessionCount + 1,
            estimationCount: dbSession.estimationCount + 1,
            updated_at: new Date(),
          })
        session.counted = true
      }
      this.copyEstimationsIntoSession(session)
      Logger.debug('Sending out session: %o', session)
      this.updateSessionForUsers(session.code)
    } catch (error) {
      Logger.error('Error revealing estimations', error)
      return callback({ error: error.message || 'Error revealing estimations' })
    }
  }

  private static copyEstimationsIntoSession(session: any) {
    const users = session.user
    const estimations = session.estimationStore
    // Set '-' for users which did not set an estimation
    if (users.length > Object.keys(estimations).length) {
      for (const user of users) {
        if (!estimations[user] && !session.spectators.includes(user)) {
          estimations[user] = '-'
        }
      }
    }
    session.estimations = estimations
  }

  public static changeDeck(socket, { sessionCode, cardDeck }, callback) {
    Logger.debug('client:changeDeck for session %s', sessionCode)
    try {
      if (callbackIsNoFunction('changeDeck', { sessionCode, cardDeck }, callback)) return
      const session = this.getSession(sessionCode)
      if (!session) {
        throw new Error('Session not found')
      }
      if (new Set([socket['name'], session.creator]).size !== 1) {
        throw new Error('You are not the hoster!')
      }
      session.revealed = false
      session.estimations = {}
      session.cardDeck = cardDeck
      session.estimationStore = {}
      this.updateSessionForUsers(sessionCode)
    } catch (error) {
      return callback({ error: error.message || 'Error setting new card deck' })
    }
  }

  public static newVoting(socket, { sessionCode }, callback) {
    Logger.debug('client:newVoting for session %s', sessionCode)
    try {
      if (callbackIsNoFunction('newVoting', { sessionCode }, callback)) return
      const session = this.getSession(sessionCode)
      if (!session) {
        throw new Error('Session not found')
      }
      if (new Set([socket['name'], session.creator]).size !== 1) {
        throw new Error('You are not the hoster!')
      }
      session.revealed = false
      session.estimations = {}
      session.estimationStore = {}
      this.updateSessionForUsers(sessionCode)
    } catch (error) {
      return callback({ error: error.message || 'Error setting new voting' })
    }
  }

  public static joinSession(socket, { name, sessionCode }, callback) {
    Logger.debug('client:joinSession from %s for session %s', name, sessionCode)
    try {
      if (callbackIsNoFunction('joinSession', { name, sessionCode }, callback)) return
      if (!name || !name.trim()) {
        throw new Error('Name required')
      }
      if (typeof {}[name.trim()] !== 'undefined') {
        throw new Error('This name is not allowed')
      }
      if (typeof {}[sessionCode.trim()] !== 'undefined') {
        throw new Error('The session code is invalid')
      }
      let session
      let sessionCreated = false
      try {
        session = this.getSession(sessionCode)
      } catch (error) {
        if (error.message !== 'Session not found') {
          throw error
        }
        Logger.debug('Creating session...')
        session = this.createSession(sessionCode, [], name)
        sessionCreated = true
      }
      if (!sessionCreated) {
        if (session.user.includes(name)) {
          throw new Error('Name already taken')
        }
        session.user.push(name)
      }
      this.addUserToSession(socket, sessionCode, name)
      this.updateSessionForUsers(sessionCode)
      callback({ name, session: session })
    } catch (error) {
      Logger.error(`Error joining session: ${error.message}`)
      return callback({ error: error.message })
    }
  }

  public static reconnectSocket(socket, { name, sessionCode }, callback) {
    Logger.debug('client:reconnectSocket from %s for session %s', name, sessionCode)
    try {
      if (callbackIsNoFunction('reconnectSocket', { name, sessionCode }, callback)) return
      this.addUserToSession(socket, sessionCode, name)
      const session = this.getSession(sessionCode)
      if (session.user.includes(name)) {
        throw new Error(
          'Someone stole your name while you have been away! You can rejoin with a new name.'
        )
      }
      session.user.push(name)
      this.updateSessionForUsers(sessionCode)
      callback()
    } catch (error) {
      Logger.error(`Error reconnecting session: ${error.message}`)
      return callback({ error: error.message })
    }
  }

  public static startTimer(socket, { sessionCode, minutes }, callback) {
    Logger.debug(
      'client:startTimer for %s minutes from %s for session %s',
      minutes,
      socket['name'],
      sessionCode
    )
    try {
      if (callbackIsNoFunction('startTimer', { sessionCode, minutes }, callback)) return
      const session = this.getSession(sessionCode)
      if (socket['name'] !== session.creator) {
        Logger.debug('User not hoster')
        throw new Error('You are not the hoster!')
      }
      var timerEndDate = new Date(new Date().getTime() + minutes * 60000)
      this.broadcastToGroup(sessionCode, 'server:startTimer', { timerEndDate })
    } catch (error) {
      Logger.error(`Error starting timer: ${error.message}`)
      return callback({ error: error.message || 'Error starting timer' })
    }
  }

  public static stopTimer(socket, { name, sessionCode }, callback) {
    Logger.debug('client:stopTimer from %s for session %s', name, sessionCode)
    try {
      if (callbackIsNoFunction('stopTimer', { name, sessionCode }, callback)) return
      const session = this.getSession(sessionCode)
      if (socket['name'] !== session.creator) {
        Logger.debug('User not hoster')
        throw new Error('You are not the hoster!')
      }
      this.broadcastToGroup(sessionCode, 'server:stopTimer', {})
    } catch (error) {
      Logger.error(`Error stopping timer: ${error.message}`)
      return callback({ error: error.message || 'Error stopping timer' })
    }
  }

  public static makeHost(socket, { user, sessionCode }, callback) {
    Logger.debug('client:makeHost user %s for session %s', user, sessionCode)
    try {
      if (callbackIsNoFunction('makeHost', { user, sessionCode }, callback)) return
      const session = this.getSession(sessionCode)
      if (socket['name'] !== session.creator) {
        Logger.debug('User not hoster')
        throw new Error('You are not the hoster!')
      }
      session.creator = user
      this.updateSessionForUsers(session.code)
    } catch (error) {
      Logger.error(`Error making new host timer: ${error.message}`)
      return callback({ error: error.message || 'Error making new host timer' })
    }
  }

  public static socketDisconnected(socket) {
    Logger.debug('disconnect')
    const sessionCode = socket['sessionCode']
    if (!sessionCode) {
      Logger.debug('no session code')
      return
    }
    let session
    try {
      session = this.getSession(sessionCode)
    } catch (error) {
      Logger.debug('no session')
      return
    }
    if (!session) {
      Logger.debug('no session')
      return
    }
    const userName = socket['name']
    session.user = session.user.filter((sessionUser) => sessionUser !== userName)
    if (userName === session.creator) {
      session.creator = session.user[0]
    }
    delete session.estimations[userName]
    socket.leave(session.code)
    if (session.user.length === 0) {
      Logger.debug('Deleting session')
      delete this.sessionStore[sessionCode]
      delete this.estimationStore[sessionCode]
    } else {
      this.updateSessionForUsers(session.code)
    }
  }
}
