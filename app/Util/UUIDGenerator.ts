const createUUID = () => {
  let d = new Date().getTime()
  return 'xxxx-xxxx'.replace(/[x]/g, () => {
    let r = Math.random() * 16
    r = (d + r) % 16 | 0
    d = Math.floor(d / 16)
    return r.toString(16)
  })
}

export { createUUID }
