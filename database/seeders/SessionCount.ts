import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Database from '@ioc:Adonis/Lucid/Database'
import Logger from '@ioc:Adonis/Core/Logger'

export default class extends BaseSeeder {
  public async run() {
    Logger.info('Seeding session_counts')

    const dbSession = (await Database.from('session_counts').select('*'))[0]

    if (dbSession?.id) {
      Logger.info('session_counts already seeded')
      return
    }

    Logger.info('Inserting session_counts')
    await Database.table('session_counts').insert([
      {
        sessionCount: 0,
        estimationCount: 0,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ])
  }
}
