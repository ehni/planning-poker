import Ws from 'App/Services/Ws'
import SocketsController from 'App/Controllers/Socket/SocketsController'

Ws.boot()

Ws.io.on('connection', (socket) => {
  socket.on('client:createSession', (data, callback) => {
    SocketsController.createNewSession(socket, data, callback)
  })

  socket.on('client:submitEstimation', (data, callback) => {
    SocketsController.submitEstimation(socket, data, callback)
  })

  socket.on('client:reveal', (data, callback) => {
    SocketsController.revealEstimations(socket, data, callback)
  })

  socket.on('client:newVoting', (data, callback) => {
    SocketsController.newVoting(socket, data, callback)
  })

  socket.on('client:joinSession', (data, callback) => {
    SocketsController.joinSession(socket, data, callback)
  })

  socket.on('client:reconnect', (data, callback) => {
    SocketsController.reconnectSocket(socket, data, callback)
  })

  socket.on('client:toggleSpectatorMode', (data, callback) => {
    SocketsController.toggleSpectatorMode(socket, data, callback)
  })

  socket.on('client:startTimer', (data, callback) => {
    SocketsController.startTimer(socket, data, callback)
  })

  socket.on('client:stopTimer', (data, callback) => {
    SocketsController.stopTimer(socket, data, callback)
  })

  socket.on('client:changeDeck', (data, callback) => {
    SocketsController.changeDeck(socket, data, callback)
  })

  socket.on('client:makeHost', (data, callback) => {
    SocketsController.makeHost(socket, data, callback)
  })

  socket.on('disconnect', () => SocketsController.socketDisconnected(socket))
})
